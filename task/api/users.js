var mysql = require('mysql');
var crypto = require("crypto");
var db_config = require("./db_config");
var mysql_config = db_config.db_config;
function sha256(text) {
	var sha256 = crypto.createHash("sha256");
	sha256.update(text, "utf8");//utf8 here
	return sha256.digest("hex");
}


exports.create = function(request, response) {
	var user = {
		username: request.body.username,
		password: request.body.password,
		email: request.body.email,
		phone: request.body.phone
	};
	var success = true;
	var connection = mysql.createConnection({
	  host     : mysql_config.host,
	  user     : mysql_config.user,
	  password : mysql_config.password,
	  database : mysql_config.database
	});
	connection.connect(function(err){
		if (!err) {
		    console.log("Database is connected ... \n\n");
		    return connection;
		} else {
		    console.log("Error connecting database ... \n\n");  
		}
	});
	connection.query('INSERT INTO users SET ?', user, function(err, result) {
		if (err) {
			success = false;
			throw err;
		}
		success = true;
	});
	connection.end();
	if (success) {
		response.setHeader('Content-Type', 'application/json');
    	response.end(JSON.stringify({'success': true}));
	}
}

exports.login = function(request, response) {
	var user = {
		username: request.body.username,
		password: request.body.password		
	};
	var result;
	var success = false;
	var connection = mysql.createConnection({
	  host     : mysql_config.host,
	  user     : mysql_config.user,
	  password : mysql_config.password,
	  database : mysql_config.database
	});
	connection.connect(function(err){
		if (!err) {
		    console.log("Database is connected ... \n\n");
		    return connection;
		} else {
		    console.log("Error connecting database ... \n\n");  
		}
	});
	connection.query('SELECT * FROM users WHERE username = ? LIMIT 1', [user.username], function(err, rows, fields) {
		var user_row = null;
		if (!err) {
			if (rows.length > 0) {
				user_row = rows[0];
				if (user_row.password != user.password) {
					success = false;
				} else {
					success = true;
				}
			}
		} else {
			success = false;
		}
		if (success == false) {
			result = {
				'success': false,
				'error': {
					'code': 123,
					'message': 'Invalid username or password'
				}
			}
			response.setHeader('Content-Type', 'application/json');
   			response.end(JSON.stringify(result));
		} else {
			var sessionId = sha256(user_row.username+user_row.password);
			result = {
				'success': true,
				'session-id': sessionId		
			}
			response.setHeader('Content-Type', 'application/json');
   			response.end(JSON.stringify(result));
		}
	});
	connection.end();
}

exports.changepass = function(request, response) {

	var user = {
		'username': request.body.username,
		'session-id': request.body['session-id'],
		'new-password': request.body['new-password']		
	};
	var success;
	var connection = mysql.createConnection({
	  host     : mysql_config.host,
	  user     : mysql_config.user,
	  password : mysql_config.password,
	  database : mysql_config.database
	});
	connection.connect(function(err){
		if (!err) {
		    console.log("Database is connected ... \n\n");
		    return connection;
		} else {
		    console.log("Error connecting database ... \n\n");  
		}
	});
	connection.query('SELECT * FROM users WHERE username = ? LIMIT 1', [user['username']], function(err, rows, fields) {
		var user_row = null;
		if (!err) {
			if (rows.length > 0) {
				user_row = rows[0];
				var sessionId = sha256(user_row.username+user_row.password);
				if (sessionId != user['session-id']) {
					success = false;
				} else {
					success = true;
				}
			} else {
				success = false;
			}		
		} else {
			success = false;
		}
		if (success == false) {
			result = {
				'success': false,
				'error': {
					'code': 34567,
					'message': 'Invalid session id'
				}
			}
			response.setHeader('Content-Type', 'application/json');
	    	response.end(JSON.stringify(result));
			connection.end();

		} else {
			// update user password
			connection.query('UPDATE users SET password = "'+user['new-password']+'" WHERE username = "'+user['username']+'"', function(err, result){
				if (err) {
					console.log(err);
				}
				connection.end();
			});
			var sessionId = sha256(user['username']+user['new-password']);
			result = {
				'success': true,
				'session-id': sessionId
			}
			response.setHeader('Content-Type', 'application/json');
	    	response.end(JSON.stringify(result));
		}	
	});

}


exports.remove = function(request, response) {
	var user = {
		'username': request.body.username,
		'session-id': request.body['session-id'],
	};
	var success;
	var connection = mysql.createConnection({
	  host     : mysql_config.host,
	  user     : mysql_config.user,
	  password : mysql_config.password,
	  database : mysql_config.database
	});
	connection.connect(function(err){
		if (!err) {
		    console.log("Database is connected ... \n\n");
		    return connection;
		} else {
		    console.log("Error connecting database ... \n\n");  
		}
	});
	connection.query('SELECT * FROM users WHERE username = ? LIMIT 1', [user['username']], function(err, rows, fields) {
		var user_row = null;
		if (!err) {
			if (rows.length > 0) {
				user_row = rows[0];
				var sessionId = sha256(user_row.username+user_row.password);
				if (sessionId != user['session-id']) {
					success = false;
				} else {
					success = true;
				}
			} else {
				success = false;
			}
			
		} else {
			success = false;
		}
		if (success == false) {
			result = {
				'success': false,
				'error': {
					'code': 2432,
					'message': 'Invalid session id'
				}
			}
			response.setHeader('Content-Type', 'application/json');
	    	response.end(JSON.stringify(result));
			connection.end();
		} else {
			// update user password
			connection.query('DELETE FROM users WHERE username = ?', [user['username']], function(err, result){
				connection.end();
			});
			result = {
				'success': true,
			}
			response.setHeader('Content-Type', 'application/json');
	    	response.end(JSON.stringify(result));
		}	
	});
	
}