var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var users = require('./api/users');

app.use(express.static(__dirname));
app.use(bodyParser.urlencoded({ extended: false }));


// app.get("/",function(req,res){
// 	connection.query('SELECT * from user LIMIT 2', function(err, rows, fields) {
// 		if (!err) {
// 			console.log('The solution is: ', rows);
// 		}
// 		else {
// 			console.log('Error while performing Query.');
// 		}
// 	});
// 	connection.end();
// });


app.post('/api/users', users.create);
app.post('/api/users/login', users.login);
app.post('/api/users/password', users.changepass);
app.post('/api/users/remove', users.remove);

app.listen(8080);