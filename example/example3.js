function Duck(name) {
	this.name = name;
	this.setName = function(name) {
		this.name = name;
	}
	this.getName = function() {
		return this.name;
	}
}

function DancingDuck() {};
DancingDuck.prototype = new Duck();
DancingDuck.prototype.dance = function() {
	console.log(this.name+' can danc');
}


function QuackingDuck() {};
QuackingDuck.prototype = new Duck();
QuackingDuck.prototype.quack = function() {
	console.log(this.name+' can quack');
}

function SingingDuck() {};
SingingDuck.prototype = new DancingDuck();
SingingDuck.prototype.quack = QuackingDuck.prototype.quack;