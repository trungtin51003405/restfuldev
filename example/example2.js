function Point(x, y) {
	this.x = x;
	this.y = y;
	this.setX = function (x) {
		this.x = x;
	}
	this.setY = function (y) {
		this.y = y;
	}
	this.getX = function () {
		return this.x;
	}
	this.getY = function () {
		return this.y;
	}
}

function Line(pointA, pointB) {
	this.pointA = pointA;
	this.pointB = pointB;
	this.getDistance = function() {
		return Math.sqrt(Math.pow(pointA.x-pointB.x, 2) + Math.pow(pointA.y-pointB.y, 2));
	}
}
// Shape
var Shape = function() {
	this.calcPerimeter = function() {};
	this.calcArea = function() {};
}
// Triangle
function Triange(lineAB, lineBC, lineCA) {
	this.lineAB = lineAB;
	this.lineBC = lineBC;
	this.lineCA = lineCA;
}
Triange.prototype = Object.create(Shape.prototype);
Triange.prototype.calcPerimeter = function() {
	return this.lineAB.getDistance() + this.lineBC.getDistance() + this.lineCA.getDistance();
}
Triange.prototype.calcArea = function() {
	var p = this.calcPerimeter()/2;
	var a = this.lineBC.getDistance();
	var b = this.lineCA.getDistance();
	var c = this.lineAB.getDistance();
	return Math.sqrt(p*(p-a)*(p-b)*(p-c));
}
// Rectangle
function Rectangle(lineAB, lineBC, lineCD, lineDA) {
	this.lineAB = lineAB;
	this.lineBC = lineBC;
	this.lineCD = lineCD;
	this.lineDA = lineDA;
}
Rectangle.prototype = Object.create(Shape.prototype);
Rectangle.prototype.calcPerimeter = function() {
	return 2 * (this.lineAB.getDistance() + this.lineBC.getDistance());
};
Rectangle.prototype.calcArea = function() {
	return this.lineAB.getDistance() * this.lineBC.getDistance();
};
// Circle
function Circle(pointC, radius) {
	this.pointC = pointC;
	this.radius = radius;
}
Circle.prototype = Object.create(Shape.prototype);
Circle.prototype.calcPerimeter = function() {
	return 2 * this.radius * Math.PI;
}
Circle.prototype.calcArea = function() {
	return this.radius*this.radius*Math.PI;
}

var circle = new Circle(new Point(0,0), 10);
console.log(circle.calcArea());
console.log(circle instanceof Shape);
