/*
 * F0 = 1, F1 = 1, F2 = 2, F3 = 3, ...
 */
exports.fibo_loop = function(n) {
	if (n < 2) {
		return 1;		
	} else {
		Fn = 0;
		Fn_1 = 1;
		Fn_2 = 1;
		for (var i = 2; i <= n; i++) {
			Fn = Fn_1 + Fn_2;
			Fn_2 = Fn_1;
			Fn_1 = Fn;
		}
		return Fn;
	} 
}

exports.fibo_recursive = function fibo_recursive(n) {
	if (n < 2) {
		return 1;
	} else {
		return fibo_recursive(n-1) + fibo_recursive(n-2);
	}
}

/*
 *
 */
exports.is_prime = function(n) {
	if (Number(n) === n) {
		if (n >= 2) {
			var is_prime = true;
			for (var i = 2; i <= Math.floor(Math.sqrt(n)); i++) {
				if (n % i == 0) {
					is_prime = false;
					break;
				}
			}
			return is_prime;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/*
 *
 */
exports.count_num_words = function(s) {
	s = s.trim();
	s = s.replace(/\s+/g, ' ');
	return s.split(' ').length;
}

/*
 *
 */

exports.megre_arrays = function(arr1, arr2) {
	var result = [];
	var associative_arr = [];
	for (var i = 0; i < arr1.length; i++) {
		if (associative_arr[arr1[i].toUpperCase()] === undefined) {
			associative_arr[arr1[i].toUpperCase()] = 1;
			result.push(arr1[i]);
		}
	}
	for (var i = 0; i < arr2.length; i++) {
		if (associative_arr[arr2[i].toUpperCase()] === undefined) {
			associative_arr[arr2[i].toUpperCase()] = 1;
			result.push(arr2[i]);
		}
	}
	return result;
}